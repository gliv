#!/bin/sh

set -o xtrace

if [ ! -f gliv-image_nr.glade ] || [ ! -d ../../../src/include ]; then
    echo "Wrong directory"
    exit 1
fi

: Cleaning...
rm -v *.[ch]

:
: Gladeing...
glade-2 -w gliv-image_nr.glade

:
: Greping
grep -vE '^#include "[^"]+"$' glade_image_nr.c > glade_image_nr.c.ttmmpp

:
: Emptying...
mv -v glade_image_nr.c.ttmmpp glade_image_nr.c
echo '/* Empty */' > empty_support.h



:
: Copying...
cp -v glade_image_nr.c ../../../src
cp -v glade_image_nr.h ../../../src/include
