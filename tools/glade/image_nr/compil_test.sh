#!/bin/sh

gcc -Wall -DPACKAGE_DATA_DIR=\".\" -DPACKAGE="" -D'_(str)=(str)' \
    -D'add_pixmap_directory(dir)=do {} while (0)'                \
    -D'create_pixmap(a, b)=gtk_image_new()'                      \
    $(pkg-config --cflags --libs gtk+-2.0)                       \
    -include image_nr_callbacks.h                                \
    glade_image_nr.c main.c image_nr_callbacks.c -o glade_image_nr
