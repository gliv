#!/bin/sh

set -o xtrace

if [ ! -f gliv-actions.glade ] || [ ! -d ../../../src/include ]; then
    echo "Wrong directory"
    exit 1
fi

: Cleaning...
rm -v *.[ch]

:
: Gladeing...
glade-2 -w gliv-actions.glade

:
: Greping
grep -vE '^#include "[^"]+"$' glade_actions.c > glade_actions.c.ttmmpp

:
: Emptying...
mv -v glade_actions.c.ttmmpp glade_actions.c
echo '/* Empty */' > empty_support.h



:
: Copying...
cp -v glade_actions.c ../../../src
cp -v glade_actions.h ../../../src/include
