#!/bin/sh

gcc -Wall -DPACKAGE_DATA_DIR=\".\" -DPACKAGE="" -D'_(str)=(str)' \
    -D'add_pixmap_directory(dir)=do {} while (0)'                \
    $(pkg-config --cflags --libs gtk+-2.0)                       \
    -include actions_callbacks.h                                 \
    glade_actions.c main.c actions_callbacks.c -o glade_actions
