#!/bin/sh

gcc -Wall -DPACKAGE_DATA_DIR=\".\" -DPACKAGE=""               \
    -DTEST_GLADE_OPTIONS                                      \
    -D'_(str)=(str)'                                          \
    -D'add_mnemonic(str)=(str)'                               \
    -D'add_check_button(button, option)=do {} while (0)'      \
    -D'add_spin_button(button, option)=do {} while (0)'       \
    -D'add_color_selection(select, option)=do {} while (0)'   \
    -D'add_button(button, option)=do {} while (0)'            \
    -D'add_cancel_button(button)=do {} while (0)'             \
    -D'add_ok_button(button)=do {} while (0)'                 \
    -D'add_pixmap_directory(dir)=do {} while (0)'             \
    -D'reset_mnemonics()=do {} while (0)'                     \
    -D'on_save_now_clicked=gtk_false'                         \
    $(pkg-config --cflags --libs gtk+-2.0)                    \
    glade_options.c main.c                                    \
    -o glade_options
