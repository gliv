#!/bin/sh

set -o xtrace

if [ ! -f gliv-options.glade ] || [ ! -d ../../../src/include ]; then
    echo "Wrong directory"
    exit 1
fi

: Cleaning...
rm -v *.[ch]

:
: Gladeing...
glade-2 -w gliv-options.glade

:
: AWKing...
awk -f buttons.awk < glade_options.c > glade_options.c.ttmmpp

:
: Emptying...
mv -v glade_options.c.ttmmpp glade_options.c
rm -v empty_*
echo '/* Empty */' > empty_support.h

:
: Copying...
cp -v glade_options.c ../../../src
cp -v glade_options.h ../../../src/include
