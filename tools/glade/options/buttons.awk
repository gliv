/\#include \"empty/ {
    next;
}

/gtk_check_button_new_with_mnemonic/ {
    sub(/_\(\"[^\"]+\"\)/, "add_mnemonic(&)");
    print;
    printf "  add_check_button(%s, OPTION_%s);\n", $1, toupper($1);
    next;
}

/gtk_spin_button_new/ {
    print;
    printf "  add_spin_button(%s, OPTION_%s);\n", $1, toupper($1);
    next;
}

/gtk_color_button_new/ {
    print;
    printf "  add_color_button(%s, OPTION_%s);\n", $1, toupper($1);
    next;
}

/gtk_container_add\ \(GTK_CONTAINER\ \(notebook1\),/ {
    print;
    printf "  reset_mnemonics();\n"
    next;
}

{
    print;
}
