#!/bin/sh

[ "$#" = "1" ] || exit 1

FORMATS_FILE=$1

DECOMP_EXTS=$(grep LOADER_DECOMP $FORMATS_FILE | cut -d, -f1)
NORMAL_EXTS=$(grep -E 'LOADER_(PIXBUF|DOT_GLIV)' $FORMATS_FILE | cut -d, -f1)
EXTS=$NORMAL_EXTS

for d in $DECOMP_EXTS
    do for n in $NORMAL_EXTS
        do EXTS="$EXTS $n\.$d"
    done
done

echo -n "'*:images:_files -/ -g \*.\(\#i\)\("
echo -n $EXTS | sed 's# #\\|#g'
echo -n "\)'"
echo
