.TH "gliv" "1" "1.9" "Guillaume Chazarain" "Image viewer"
.SH "NAME"
gliv \- An OpenGL Image Viewer
.SH "SYNOPSIS"
\fBgliv\fR [OPTIONS]... [FILES]...
.SH "DESCRIPTION"
\fIgliv\fR uses gdk\-pixbuf to load images and OpenGL to render them.
It allows to do some moving, rotating, zooming and slide show.
.SH "OPTIONS"
The options are first set to their default values, "off" for flags, then read either from ~/.glivrc or /etc/glivrc or a configuration file specified on the command line, and finally read from the command line.
.br 
Omitting the argument for an option that takes an "on|off" argument (flags) is like giving it "on", so \-\-foo is the same as \-\-foo=on if foo is an on|off flag.
.TP 
\fB\-h, \-\-help\fR
Print help and exit.
.TP 
\fB\-V, \-\-version\fR
Print version and exit.
.TP 
\fB\-a, \-\-add\-all[=on|off]\fR
Add all files in the directory. With this option, when you open a file on the command line or with the open dialog, all others files in the directory are added as well.
.TP 
\fB\-R, \-\-recursive[=on|off]\fR
Recursive directory traversal. If \fIgliv\fR tries to load a direcory, it will also load every image all its subdirectories.
.TP 
\fB\-S, \-\-sort[=on|off]\fR
Show images in sorted order. The images list will be sorted before the slide show.
.TP 
\fB\-s, \-\-shuffle[=on|off]\fR
Show images in random order. The images list will be shuffled before the slide show.
.TP 
\fB\-F, \-\-force\-load[=on|off]\fR
Try to load every file. When loading a file, the loader is chosen according to the filename extension in order to optimize the loading time. If the extension is unknown, the file is ignored, with this option \fIgliv\fR will always try to load the file.
.TP 
\fB\-C, \-\-client[=on|off]\fR
Connect to a running \fIgliv\fR, appending to the list. With this option, \fIgliv\fR will open the files passed in the arguments in the \fIgliv\fR server window. It can also be used with the \-0 option. The \fIgliv\fR server is the latest launched \fIgliv\fR or the one which has been chosen using the Options menu.
.TP 
\fB\-c, \-\-client-clear[=on|off]\fR
Connect to a running \fIgliv\fR, replacing the list. This is like the \-\-client option except that the specified list on the client will replace the list on the server instead of being appended to.
.TP 
\fB\-e, \-\-build\-menus[=on|off]\fR
No images menu at startup. Disabling the images menus creation at startup makes it faster, especially with many files on the command line.
.TP 
\fB\-g, \-\-glivrc[=FILE]\fR
Use this configuration file or none. Specify it to disable loading of the rc file. With a filename as argument it will use it as a configuration file.
.TP 
\fB\-w, \-\-slide\-show[=on|off]\fR
Start the slide show immediately. This way you will not have to start it manually from the menu.
.TP 
\fB\-0, \-\-null[=on|off]\fR
Read null\-terminated filenames. This can be used with "find \-print0" or with "tr '\en' '\e0'" when you have a very long list of filenames to pass to \fIgliv\fR. Unlike xargs(1) it allows an unlimited number of filenames.
.TP 
\fB\-o, \-\-collection[=FILE]\fR
Output a collection. With this option, \fIgliv\fR creates a collection from the loaded files and outputs it to stdout or in the specified file.
.TP 
\fB\-G, \-\-geometry=GEOMETRY\fR
Initial window geometry. This option can be used to specify the position and dimension of the \fIgliv\fR window. It expects a geometry argument in the XParseGeometry(3X11) format like: 640x480+20\-30 for example.
.SH "COLLECTIONS"
Starting with version 1.8, \fIgliv\fR supports a file format called "GLiv collection". It contains an images list and the associated thumbnails, this way, when you load a collection the images menus rebuilding is faster since it does not have to make thumbnails.
.br 
\fIgliv\fR supports also transparent decompression, so the collections can be compressed in bzip2, gzip or compress(1) provided that  you have the corresponding decompressor.
.SH "CONTROLS"

