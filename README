o GLiv
~~~~~~

GLiv is an OpenGL image viewer, image loading
is done via Gdk-pixbuf bundled with GTK+-2.6,
rendering  with OpenGL and the graphical user
interface uses GTK+ with GtkGLExt.
GLiv  is  very  fast  and smooth at rotating,
panning  and  zooming  if  you have an OpenGL
accelerated graphics board.


o Requirements
~~~~~~~~~~~~~~

- OpenGL
- GTK+ >= 2.6
- GtkGLExt >= 0.7.0


o Compilation
~~~~~~~~~~~~~

The usual

$ ./configure
with your prefix path, then

$ make

If  you  are using the Nvidia OpenGL drivers,
be  sure they are actually used: once GLiv is
compiled,

$ ldd src/gliv | grep 'libGL\.so'

should show the path of the libGL.so from the
Nvidia drivers.


o Installation
~~~~~~~~~~~~~~

A  'make  install'  as root will install gliv
in /usr/local or in your prefix path.


o Controls
~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ESC, q : Quit
f      : Full-screen/window
+/=/-  : Zoom in/in/out
n/p    : Next/previous image
Pause  : Start/stop the slide show
l      : Reduce the image to the window
M      : Maximize the image to the window
m      : Make the image fit the window
r      : Reset position and size
b      : Toggle display of the menu bar
i      : Toggle display of the info bar
s      : Toggle display of the scrollbars
a      : Toggle display of the alpha checks
h      : Toggle display of the help box
w      : Toggle display of floating windows
o      : Display the open dialog
g      : Display the image selector
t      : Display the options dialog
d      : Hide the cursor
u      : Undo
y      : Redo
c      : Clear the history
Delete : Delete the current file
C-up   : Rotate by +90 degrees
C-down : Rotate by -90 degrees
C-left : Rotate by +0.1 degree
C-right: Rotate by -0.1 degree
z      : Horizontal flip
e      : Vertical flip

The  first  mouse  button  and the arrow keys
will move the image unless the Control key is
pressed.  In  which  case  the  image will be
rotated around the window center.

The mouse wheel zooms the image, and when you
hold  its  button pressed at the same time it
switches to the neighbouring image.

You  can  also  zoom  by  dragging  the mouse
vertically  while holding Shift and the first
button.

Space and Backspace act like n and p.

Draw  a  rectangle  with the third button and
gliv will zoom in it.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The  help  box contains the above text, maybe
translated.

Further  informations  can  be  found  in the
manual page.



Guillaume Chazarain <guichaz@gmail.com>
http://guichaz.free.fr/gliv
