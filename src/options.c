/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * See the COPYING file for license information.
 *
 * Guillaume Chazarain <guichaz@gmail.com>
 */

/*****************************
 * User configurable options *
 *****************************/

#include <string.h>             /* memcmp() */
#include <gdk/gdkkeysyms.h>     /* GDK_Escape, GDK_c, GDK_C, GDK_o, GDK_O */

#include "gliv.h"
#include "options.h"
#include "glade_options.h"
#include "gliv-image.h"
#include "gl_widget.h"
#include "history.h"
#include "windows.h"
#include "matrix.h"
#include "messages.h"
#include "rendering.h"
#include "scrollbars.h"
#include "next_image.h"
#include "rcfile.h"
#include "mnemonics.h"
#include "images_menus.h"

extern rt_struct *rt;
extern options_struct *options;
extern GlivImage *current_image;
extern GtkWidget *gl_widget;

/* Filled by the dialog. */
static options_struct *new_options;

static void toggle_delay(void)
{
    if (options->delay == 0)
        /* Enable. */
        gtk_widget_add_events(gl_widget, GDK_POINTER_MOTION_MASK);

    options->delay = new_options->delay;
    schedule_hide_cursor();
}

static void toggle_history(void)
{
    options->history_size = new_options->history_size;
    clean_history();
}

static void apply(void)
{
    gint what = 0;
    gboolean bg_color_changed;

    if (options->fullscreen != new_options->fullscreen)
        toggle_fullscreen(new_options->fullscreen);

    if ((options->maximize == FALSE && new_options->maximize) ||
        (options->scaledown == FALSE && new_options->scaledown)) {

        options->maximize = new_options->maximize;
        options->scaledown = new_options->scaledown;

        matrix_set_max_zoom(-1, -1, TRUE);
        what |= REFRESH_IMAGE | REFRESH_STATUS | APPEND_HISTORY;
    }

    if (options->menu_bar != new_options->menu_bar)
        toggle_menu_bar();

    if (options->status_bar != new_options->status_bar)
        toggle_status_bar();

    if (options->scrollbars != new_options->scrollbars)
        toggle_scrollbars();

    if (options->dither != new_options->dither ||
        options->mipmap != new_options->mipmap) {

        options->dither = new_options->dither;
        options->mipmap = new_options->mipmap;

        reload_images();
        what |= REFRESH_IMAGE;
    }

    if (options->one_image == FALSE && new_options->one_image)
        unload_images();

    if (options->delay != new_options->delay)
        toggle_delay();

    if (options->history_size != new_options->history_size)
        toggle_history();

    if (options->duration != new_options->duration)
        if (slide_show_started()) {
            options->duration = new_options->duration;
            start_slide_show();
        }

    if (options->filtering != new_options->filtering)
        what |= REFRESH_IMAGE;

    if (options->thumbnails != new_options->thumbnails ||
        options->thumb_width != new_options->thumb_width ||
        options->thumb_height != new_options->thumb_height ||
        options->mnemonics != new_options->mnemonics)
        obsolete_menus();

    bg_color_changed = !gdk_color_equal(&options->bg_col, &new_options->bg_col);

    if (!gdk_color_equal(&options->alpha1, &new_options->alpha1) ||
        !gdk_color_equal(&options->alpha2, &new_options->alpha2)) {

        rt->alpha_checks_changed = TRUE;

        if (current_image != NULL && current_image->has_alpha)
            what |= REFRESH_IMAGE;
    }

    g_free(options);
    options = new_options;

    if (bg_color_changed) {
        update_bg_color();
        what |= REFRESH_IMAGE;
    }

    refresh(what);
}

/* GtkColorButton */

static void add_color_button(GtkWidget * color_button, GdkColor * col)
{
    gtk_color_button_set_color(GTK_COLOR_BUTTON(color_button), col);

    g_signal_connect(color_button, "color-set",
                     G_CALLBACK(gtk_color_button_get_color), col);
}

/* GtkSpinButton */

static gboolean value_changed(GtkAdjustment * adj, gint * val)
{
    *val = (gint) gtk_adjustment_get_value(adj);
    return TRUE;
}

static void add_spin_button(GtkWidget * button, gint * value)
{
    GtkAdjustment *adj;

    adj = gtk_spin_button_get_adjustment(GTK_SPIN_BUTTON(button));
    gtk_adjustment_set_value(adj, (gdouble) * value);

    g_signal_connect(adj, "value-changed", G_CALLBACK(value_changed), value);
}

/* GtkCheckButton */

static gboolean toggled_button(GtkToggleButton * button, gboolean * bool)
{
    *bool = gtk_toggle_button_get_active(button);
    return TRUE;
}

static void add_check_button(GtkWidget * button, gboolean * value)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), *value);
    g_signal_connect(button, "toggled", G_CALLBACK(toggled_button), value);
}

/* GtkComboBox */

static void on_initial_position_changed(GtkComboBox * initial_position)
{
    new_options->initial_pos = gtk_combo_box_get_active(initial_position);
}

static void on_initial_position_realize(GtkComboBox * initial_position)
{
    gtk_combo_box_set_active(initial_position, options->initial_pos);
}

/* Save Options */

static gboolean on_save_now_clicked(void)
{
    save_rc(new_options);
    return FALSE;
}

static gboolean on_read_config_realize(GtkLabel * widget)
{
    const gchar *filename = get_read_config_file();
    gchar *text;

    if (filename == NULL)
        filename = _("NONE");

    text =
        g_strdup_printf(_("This configuration file has been read: %s"),
                        filename);

    gtk_label_set_text(widget, text);
    g_free(text);

    return FALSE;
}

static gboolean on_write_config_realize(GtkLabel * widget)
{
    const gchar *filename = get_write_config_file();
    gchar *text;

    if (filename == NULL)
        filename = _("NONE");

    text =
        g_strdup_printf(_("This configuration file will be written: %s"),
                        filename);

    gtk_label_set_text(widget, text);
    g_free(text);

    return FALSE;
}

/* Transitions */

static gboolean on_transitions_toggled(GtkWidget * transition_box,
                                       GtkToggleButton * button)
{
    gboolean activated = gtk_toggle_button_get_active(button);

    gtk_widget_set_sensitive(transition_box, activated);
    return FALSE;
}

static gboolean on_transition_box_realize(GtkWidget * transition_box)
{
    gtk_widget_set_sensitive(transition_box, options->transitions);
    return FALSE;
}

#include "glade_options.c"

gboolean show_options(void)
{
    gint response;
    GtkDialog *options_dialog;

    new_options = g_memdup(options, sizeof(options_struct));
    options_dialog = GTK_DIALOG(create_options_dialog());

    response = run_modal_dialog(options_dialog);
    gtk_widget_destroy(GTK_WIDGET(options_dialog));

    if (response == GTK_RESPONSE_OK)
        apply();

    return FALSE;
}
