/* -*- mode: c; c-file-style: "k&r" -*-

  strnatcmp.c -- Perform 'natural order' comparisons of strings in C.
  Copyright (C) 2000, 2004 by Martin Pool <mbp sourcefrog net>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

/* Modified by Guillaume Chazarain in order to be integrated in GLiv */

#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#include "strnatcmp.h"


static int compare_right(const char *a, const char *b)
{
    int bias = 0;

    /* The longest run of digits wins.  That aside, the greatest
       value wins, but we can't know that it will until we've scanned
       both numbers to know that they have the same magnitude, so we
       remember it in BIAS. */
    for (;; a++, b++) {
        if (!isdigit(*a))
            return isdigit(*b) ? -1 : bias;
        else if (!isdigit(*b))
            return +1;
        else if (*a < *b) {
            if (!bias)
                bias = -1;
        } else if (*a > *b) {
            if (!bias)
                bias = +1;
        } else if (!*a && !*b)
            return bias;
    }

    return 0;
}


static int compare_left(const char *a, const char *b)
{
    /* Compare two left-aligned numbers: the first to have a
       different value wins. */
    for (;; a++, b++) {
        if (!isdigit(*a))
            return !isdigit(*b) - 1;
        else if (!isdigit(*b))
            return +1;
        else if (*a < *b)
            return -1;
        else if (*a > *b)
            return +1;
    }

    return 0;
}


int strnatcmp(const char *a, const char *b)
{
    int ai, bi;
    char ca, cb;
    int fractional, result;

    ai = bi = 0;
    while (1) {
        ca = a[ai];
        cb = b[bi];

        /* skip over leading spaces or zeros */
        while (isspace(ca))
            ca = a[++ai];

        while (isspace(cb))
            cb = b[++bi];

        /* process run of digits */
        if (isdigit(ca) && isdigit(cb)) {
            fractional = (ca == '0' || cb == '0');

            if (fractional) {
                if ((result = compare_left(a + ai, b + bi)) != 0)
                    return result;
            } else {
                if ((result = compare_right(a + ai, b + bi)) != 0)
                    return result;
            }
        }

        if (!ca && !cb) {
            /* The strings compare the same.  Perhaps the caller
               will want to call strcmp to break the tie. */
            return 0;
        }

        if (ca < cb)
            return -1;
        else if (ca > cb)
            return +1;

        ++ai;
        ++bi;
    }
}
