/* ANSI-C code produced by gperf version 3.0.3 */
/* Command-line: gperf -n --multiple-iterations 64 --output formats.c ../tools/formats.gperf  */
/* Computed positions: -k'1-4' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#line 1 "../tools/formats.gperf"


#include "gliv.h"
#include "formats.h"

#include <string.h>

#define TOTAL_KEYWORDS 29
#define MIN_WORD_LENGTH 1
#define MAX_WORD_LENGTH 5
#define MIN_HASH_VALUE 0
#define MAX_HASH_VALUE 28
/* maximum key range = 29, duplicates = 0 */

#ifndef GPERF_DOWNCASE
#define GPERF_DOWNCASE 1
static unsigned char gperf_downcase[256] =
  {
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
     30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
     45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
     60,  61,  62,  63,  64,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
    122,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
    150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
    165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
    195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224,
    225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
    255
  };
#endif

#ifndef GPERF_CASE_STRCMP
#define GPERF_CASE_STRCMP 1
static int
gperf_case_strcmp (register const char *s1, register const char *s2)
{
  for (;;)
    {
      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];
      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];
      if (c1 != 0 && c1 == c2)
        continue;
      return (int)c1 - (int)c2;
    }
}
#endif

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 18, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 13,  9,  0, 29, 29,
       2,  4, 11,  0,  8, 29,  9, 11,  2, 29,
       0,  6,  8,  0,  3, 22,  6, 10,  1, 15,
       1, 29, 29, 29, 29, 29, 29, 13,  9,  0,
      29, 29,  2,  4, 11,  0,  8, 29,  9, 11,
       2, 29,  0,  6,  8,  0,  3, 22,  6, 10,
       1, 15,  1, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
      29, 29, 29, 29, 29, 29, 29
    };
  register int hval = 0;

  switch (len)
    {
      default:
        hval += asso_values[(unsigned char)str[3]];
      /*FALLTHROUGH*/
      case 3:
        hval += asso_values[(unsigned char)str[2]+1];
      /*FALLTHROUGH*/
      case 2:
        hval += asso_values[(unsigned char)str[1]];
      /*FALLTHROUGH*/
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

#ifdef __GNUC__
__inline
#ifdef __GNUC_STDC_INLINE__
__attribute__ ((__gnu_inline__))
#endif
#endif
const struct format *
ext_to_loader (register const char *str, register unsigned int len)
{
  static const struct format wordlist[] =
    {
#line 26 "../tools/formats.gperf"
      {"ico",   LOADER_PIXBUF},
#line 47 "../tools/formats.gperf"
      {"z",     LOADER_DECOMP},
#line 35 "../tools/formats.gperf"
      {"ppm",   LOADER_PIXBUF},
#line 46 "../tools/formats.gperf"
      {"xpm",   LOADER_PIXBUF},
#line 34 "../tools/formats.gperf"
      {"pnm",   LOADER_PIXBUF},
#line 25 "../tools/formats.gperf"
      {"gz",    LOADER_DECOMP},
#line 31 "../tools/formats.gperf"
      {"pgm",   LOADER_PIXBUF},
#line 41 "../tools/formats.gperf"
      {"tif",   LOADER_PIXBUF},
#line 23 "../tools/formats.gperf"
      {"gif",   LOADER_PIXBUF},
#line 42 "../tools/formats.gperf"
      {"tiff",  LOADER_PIXBUF},
#line 27 "../tools/formats.gperf"
      {"jpe",   LOADER_PIXBUF},
#line 30 "../tools/formats.gperf"
      {"pbm",   LOADER_PIXBUF},
#line 45 "../tools/formats.gperf"
      {"xbm",   LOADER_PIXBUF},
#line 33 "../tools/formats.gperf"
      {"png",   LOADER_PIXBUF},
#line 28 "../tools/formats.gperf"
      {"jpeg",  LOADER_PIXBUF},
#line 32 "../tools/formats.gperf"
      {"pcx",   LOADER_PIXBUF},
#line 40 "../tools/formats.gperf"
      {"tga",   LOADER_PIXBUF},
#line 37 "../tools/formats.gperf"
      {"svg",   LOADER_PIXBUF},
#line 38 "../tools/formats.gperf"
      {"svgz",  LOADER_PIXBUF},
#line 29 "../tools/formats.gperf"
      {"jpg",   LOADER_PIXBUF},
#line 39 "../tools/formats.gperf"
      {"targa", LOADER_PIXBUF},
#line 43 "../tools/formats.gperf"
      {"wbmp",  LOADER_PIXBUF},
#line 22 "../tools/formats.gperf"
      {"cur",   LOADER_PIXBUF},
#line 19 "../tools/formats.gperf"
      {"ani",   LOADER_PIXBUF},
#line 36 "../tools/formats.gperf"
      {"ras",   LOADER_PIXBUF},
#line 44 "../tools/formats.gperf"
      {"wmf",   LOADER_PIXBUF},
#line 20 "../tools/formats.gperf"
      {"bmp",   LOADER_PIXBUF},
#line 24 "../tools/formats.gperf"
      {"gliv",  LOADER_DOT_GLIV},
#line 21 "../tools/formats.gperf"
      {"bz2",   LOADER_DECOMP}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].name;

          if ((((unsigned char)*str ^ (unsigned char)*s) & ~32) == 0 && !gperf_case_strcmp (str, s))
            return &wordlist[key];
        }
    }
  return 0;
}
