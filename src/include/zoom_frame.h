#ifndef ZOOM_FRAME_H
#define ZOOM_FRAME_H

#include "gliv.h"

void draw_zoom_frame(void);
void set_zoom_frame(gint x, gint y, gint width, gint height);
void clear_zoom_frame(void);
void zoom_frame(void);

#endif
