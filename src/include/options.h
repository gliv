#ifndef OPTIONS_H
#define OPTIONS_H

#include "gliv.h"

/* Links between the option dialog buttons and the options values. */
#define OPTION_FULLSCREEN                (&new_options->fullscreen)
#define OPTION_MENU_BAR                  (&new_options->menu_bar)
#define OPTION_SCROLLBARS                (&new_options->scrollbars)
#define OPTION_STATUS_BAR                (&new_options->status_bar)
#define OPTION_DELAY_CURSOR              (&new_options->delay)
#define OPTION_DITHERING                 (&new_options->dither)
#define OPTION_SCALE_DOWN                (&new_options->scaledown)
#define OPTION_ALPHA_CHECKS              (&new_options->alpha_checks)
#define OPTION_ONE_IMAGE                 (&new_options->one_image)
#define OPTION_MIPMAPS                   (&new_options->mipmap)
#define OPTION_MAXIMIZE                  (&new_options->maximize)
#define OPTION_RESIZE_WIN                (&new_options->resize_win)
#define OPTION_IMAGES_MENUS_STARTUP      (&new_options->build_menus)
#define OPTION_IMAGES_MENUS_MNEMONICS    (&new_options->mnemonics)
#define OPTION_THUMBNAILS                (&new_options->thumbnails)
#define OPTION_THUMBNAILS_WIDTH          (&new_options->thumb_width)
#define OPTION_THUMBNAILS_HEIGHT         (&new_options->thumb_height)
#define OPTION_ZOOM_POINTER              (&new_options->zoom_pointer)
#define OPTION_MAX_FPS                   (&new_options->fps)
#define OPTION_HISTORY_LENGTH            (&new_options->history_size)
#define OPTION_START_SLIDE_SHOW          (&new_options->start_show)
#define OPTION_SLIDE_SHOW_LOOP           (&new_options->loop)
#define OPTION_SLIDE_SHOW_DELAY          (&new_options->duration)
#define OPTION_BACKGROUND                (&new_options->bg_col)
#define OPTION_ALPHA1                    (&new_options->alpha1)
#define OPTION_ALPHA2                    (&new_options->alpha2)
#define OPTION_NOTICE_TIME               (&new_options->notice_time)
#define OPTION_CONFIRM_QUIT              (&new_options->confirm_quit)
#define OPTION_TRANS_DURATION            (&new_options->trans_time)
#define OPTION_SAVE_QUIT                 (&new_options->save_quit)
#define OPTION_TRANSITIONS               (&new_options->transitions)
#define OPTION_KEEP_TRANSFO              (&new_options->keep_transfo)
#define OPTION_OPENGL_ERRORS             (&new_options->opengl_errors)
#define OPTION_FILTERING                 (&new_options->filtering)

/* Must be kept in sync with 'enum transfo' */
enum image_position {
    POSITION_CENTER = 0,
    POSITION_TOP_LEFT,
    POSITION_TOP_RIGHT,
    POSITION_BOTTOM_LEFT,
    POSITION_BOTTOM_RIGHT,
    POSITION_KEEP
};

/* Options controlled by the user. */
typedef struct {
    gboolean fullscreen;
    gboolean maximize;
    gboolean scaledown;
    gboolean menu_bar;
    gboolean status_bar;
    gboolean scrollbars;
    gboolean zoom_pointer;
    gboolean alpha_checks;
    gboolean dither;
    gboolean force;
    gboolean build_menus;
    gboolean mipmap;
    gboolean mnemonics;
    gboolean loop;
    gboolean one_image;
    gboolean start_show;
    gboolean thumbnails;
    gboolean resize_win;
    gboolean confirm_quit;
    gboolean save_quit;
    gboolean transitions;
    gboolean recursive;
    gboolean keep_transfo;
    gboolean opengl_errors;
    gboolean filtering;
    gint thumb_width;
    gint thumb_height;
    gint delay;
    gint duration;
    gint history_size;
    gint fps;
    gint notice_time;
    gint trans_time;
    enum image_position initial_pos;
    gchar *initial_geometry;
    GdkColor bg_col;
    GdkColor alpha1;
    GdkColor alpha2;
} options_struct;

/* Some global flags and variables. */
typedef struct {
    gboolean cursor_hidden;
    gboolean help;
    gboolean alpha_checks_changed;
    gint max_texture_size;
    GtkAllocation *wid_size;
    gint scr_width;
    gint scr_height;
} rt_struct;

gboolean show_options(void);

#endif
