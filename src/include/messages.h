#ifndef MESSAGES_H
#define MESSAGES_H

#include "gliv.h"
#include "windows.h"
#include "cursors.h"

#ifdef ENABLE_NLS
#include "gettext.h"
#include <glib/gi18n.h>
#else
#define _(a) (a)
#define N_(a) (a)
#endif

#if HAVE_ISO_C_VARARGS

#define DIALOG_MSG(...)                                            \
    do {                                                           \
        GtkMessageDialog *__dialog__;                              \
                                                                   \
        g_printerr(__VA_ARGS__);                                   \
        g_printerr("\n");                                          \
                                                                   \
        __dialog__ = GTK_MESSAGE_DIALOG(                           \
            gtk_message_dialog_new(get_current_window(),           \
                                   GTK_DIALOG_DESTROY_WITH_PARENT, \
                                   GTK_MESSAGE_WARNING,            \
                                   GTK_BUTTONS_OK,                 \
                                   __VA_ARGS__));                  \
                                                                   \
        /*                                                         \
         * We want the cursor to be visible                        \
         * when the dialog is shown.                               \
         */                                                        \
        set_hide_cursor_enabled(FALSE);                            \
        gtk_dialog_run(GTK_DIALOG(__dialog__));                    \
        set_hide_cursor_enabled(TRUE);                             \
                                                                   \
        gtk_widget_destroy(GTK_WIDGET(__dialog__));                \
    } while (0)

#else
#define DIALOG_MSG g_printerr
#warning "No variadic macros"
#endif

#endif
