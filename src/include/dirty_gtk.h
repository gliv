#ifndef DIRTY_GTK_H
#define DIRTY_GTK_H

#include "gliv.h"

typedef struct {
    GtkMenu *gtk_menu;
    GList *last_child;
    GtkTearoffMenuItem *tearoff;
} DirtyGtkMenu;

DirtyGtkMenu *dirty_gtk_menu_new(void);
void dirty_gtk_menu_append(DirtyGtkMenu * menu, GtkWidget * item);
GtkTearoffMenuItem *dirty_gtk_menu_get_tearoff(DirtyGtkMenu * menu);
void dirty_gtk_menu_release(DirtyGtkMenu * menu);

#endif
