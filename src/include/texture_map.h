#ifndef TEXTURE_MAP_H
#define TEXTURE_MAP_H

#include "gliv.h"
#include "tiling.h"

/* A rectangle in a map. */
typedef struct {
    gfloat x0;
    gfloat y0;
    gfloat x1;
    gfloat y1;
} tile_dim;

/* A map in a texture. */
typedef struct {
    gint nb_tiles;
    tile_dim *tiles;
    guint *tex_ids;
    guint list;
    gint width;
    gint height;
    struct tiles *x_tiles;
    struct tiles *y_tiles;
    GdkPixbuf *pixbuf;
} texture_map;

#endif
