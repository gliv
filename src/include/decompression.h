#ifndef DECOMPRESSION_H
#define DECOMPRESSION_H

#include "gliv.h"

GdkPixbuf *load_compressed_pixbuf(const gchar * filename, GError ** error);
gint load_compressed_collection(const gchar * filename);

#endif
