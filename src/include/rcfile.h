#ifndef RCFILE_H
#define RCFILE_H

#include "gliv.h"
#include "options.h"

options_struct *load_rc(gboolean default_file, const gchar * filename);
void load_actions(void);
void load_accelerators(void);
void save_rc(options_struct * opt);
const gchar *get_read_config_file(void);
const gchar *get_write_config_file(void);

#endif
