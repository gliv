#ifndef MAIN_H
#define MAIN_H

#include "gliv.h"

G_GNUC_NORETURN void quit(gint code);
gboolean gui_quit(void);

#endif
