#ifndef COLLECTION_H
#define COLLECTION_H

#include <stdio.h>
#include "gliv.h"

gint serialize_collection_nogui(const gchar * filename);
gboolean serialize_collection_gui(void);
gint load_dot_gliv_from_file(const gchar * filename, FILE * file);
gint load_dot_gliv(const gchar * filename);
gboolean deserialize_collection(void);

#endif
