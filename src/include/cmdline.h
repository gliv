/** @file cmdline.h
 *  @brief The header file for the command line option parser
 *  generated by GNU Gengetopt version 2.22.4
 *  http://www.gnu.org/software/gengetopt.
 *  DO NOT modify this file, since it can be overwritten
 *  @author GNU Gengetopt by Lorenzo Bettini */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h> /* for FILE */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
/** @brief the program name (used for printing errors) */
#define CMDLINE_PARSER_PACKAGE PACKAGE
#endif

#ifndef CMDLINE_PARSER_PACKAGE_NAME
/** @brief the complete program name (used for help and version) */
#ifdef PACKAGE_NAME
#define CMDLINE_PARSER_PACKAGE_NAME PACKAGE_NAME
#else
#define CMDLINE_PARSER_PACKAGE_NAME PACKAGE
#endif
#endif

#ifndef CMDLINE_PARSER_VERSION
/** @brief the program version */
#define CMDLINE_PARSER_VERSION VERSION
#endif

/** @brief Where the command line options are stored */
struct gengetopt_args_info
{
  const char *help_help; /**< @brief Print help and exit help description.  */
  const char *version_help; /**< @brief Print version and exit help description.  */
  char * add_all_arg;	/**< @brief Add all files in the directory.  */
  char * add_all_orig;	/**< @brief Add all files in the directory original value given at command line.  */
  const char *add_all_help; /**< @brief Add all files in the directory help description.  */
  char * recursive_arg;	/**< @brief Recursive directory traversal.  */
  char * recursive_orig;	/**< @brief Recursive directory traversal original value given at command line.  */
  const char *recursive_help; /**< @brief Recursive directory traversal help description.  */
  char * sort_arg;	/**< @brief Show images in sorted order.  */
  char * sort_orig;	/**< @brief Show images in sorted order original value given at command line.  */
  const char *sort_help; /**< @brief Show images in sorted order help description.  */
  char * shuffle_arg;	/**< @brief Show images in random order.  */
  char * shuffle_orig;	/**< @brief Show images in random order original value given at command line.  */
  const char *shuffle_help; /**< @brief Show images in random order help description.  */
  char * force_load_arg;	/**< @brief Try to load every file.  */
  char * force_load_orig;	/**< @brief Try to load every file original value given at command line.  */
  const char *force_load_help; /**< @brief Try to load every file help description.  */
  char * client_arg;	/**< @brief Connect to a running gliv, appending to the list.  */
  char * client_orig;	/**< @brief Connect to a running gliv, appending to the list original value given at command line.  */
  const char *client_help; /**< @brief Connect to a running gliv, appending to the list help description.  */
  char * client_clear_arg;	/**< @brief Connect to a running gliv, replacing the list.  */
  char * client_clear_orig;	/**< @brief Connect to a running gliv, replacing the list original value given at command line.  */
  const char *client_clear_help; /**< @brief Connect to a running gliv, replacing the list help description.  */
  char * build_menus_arg;	/**< @brief No images menu at startup.  */
  char * build_menus_orig;	/**< @brief No images menu at startup original value given at command line.  */
  const char *build_menus_help; /**< @brief No images menu at startup help description.  */
  char * glivrc_arg;	/**< @brief Use this configuration file or none.  */
  char * glivrc_orig;	/**< @brief Use this configuration file or none original value given at command line.  */
  const char *glivrc_help; /**< @brief Use this configuration file or none help description.  */
  char * slide_show_arg;	/**< @brief Start the slide show immediately.  */
  char * slide_show_orig;	/**< @brief Start the slide show immediately original value given at command line.  */
  const char *slide_show_help; /**< @brief Start the slide show immediately help description.  */
  char * null_arg;	/**< @brief Read null-terminated filenames.  */
  char * null_orig;	/**< @brief Read null-terminated filenames original value given at command line.  */
  const char *null_help; /**< @brief Read null-terminated filenames help description.  */
  char * collection_arg;	/**< @brief Output a collection.  */
  char * collection_orig;	/**< @brief Output a collection original value given at command line.  */
  const char *collection_help; /**< @brief Output a collection help description.  */
  char * geometry_arg;	/**< @brief Initial window geometry.  */
  char * geometry_orig;	/**< @brief Initial window geometry original value given at command line.  */
  const char *geometry_help; /**< @brief Initial window geometry help description.  */
  
  unsigned int help_given ;	/**< @brief Whether help was given.  */
  unsigned int version_given ;	/**< @brief Whether version was given.  */
  unsigned int add_all_given ;	/**< @brief Whether add-all was given.  */
  unsigned int recursive_given ;	/**< @brief Whether recursive was given.  */
  unsigned int sort_given ;	/**< @brief Whether sort was given.  */
  unsigned int shuffle_given ;	/**< @brief Whether shuffle was given.  */
  unsigned int force_load_given ;	/**< @brief Whether force-load was given.  */
  unsigned int client_given ;	/**< @brief Whether client was given.  */
  unsigned int client_clear_given ;	/**< @brief Whether client-clear was given.  */
  unsigned int build_menus_given ;	/**< @brief Whether build-menus was given.  */
  unsigned int glivrc_given ;	/**< @brief Whether glivrc was given.  */
  unsigned int slide_show_given ;	/**< @brief Whether slide-show was given.  */
  unsigned int null_given ;	/**< @brief Whether null was given.  */
  unsigned int collection_given ;	/**< @brief Whether collection was given.  */
  unsigned int geometry_given ;	/**< @brief Whether geometry was given.  */

  char **inputs ; /**< @brief unamed options (options without names) */
  unsigned inputs_num ; /**< @brief unamed options number */
} ;

/** @brief The additional parameters to pass to parser functions */
struct cmdline_parser_params
{
  int override; /**< @brief whether to override possibly already present options (default 0) */
  int initialize; /**< @brief whether to initialize the option structure gengetopt_args_info (default 1) */
  int check_required; /**< @brief whether to check that all required options were provided (default 1) */
  int check_ambiguity; /**< @brief whether to check for options already specified in the option structure gengetopt_args_info (default 0) */
  int print_errors; /**< @brief whether getopt_long should print an error message for a bad option (default 1) */
} ;

/** @brief the purpose string of the program */
extern const char *gengetopt_args_info_purpose;
/** @brief the usage string of the program */
extern const char *gengetopt_args_info_usage;
/** @brief all the lines making the help output */
extern const char *gengetopt_args_info_help[];

/**
 * The command line parser
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser (int argc, char **argv,
  struct gengetopt_args_info *args_info);

/**
 * The command line parser (version with additional parameters - deprecated)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param override whether to override possibly already present options
 * @param initialize whether to initialize the option structure my_args_info
 * @param check_required whether to check that all required options were provided
 * @return 0 if everything went fine, NON 0 if an error took place
 * @deprecated use cmdline_parser_ext() instead
 */
int cmdline_parser2 (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

/**
 * The command line parser (version with additional parameters)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param params additional parameters for the parser
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_ext (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  struct cmdline_parser_params *params);

/**
 * Save the contents of the option struct into an already open FILE stream.
 * @param outfile the stream where to dump options
 * @param args_info the option struct to dump
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_dump(FILE *outfile,
  struct gengetopt_args_info *args_info);

/**
 * Save the contents of the option struct into a (text) file.
 * This file can be read by the config file parser (if generated by gengetopt)
 * @param filename the file where to save
 * @param args_info the option struct to save
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

/**
 * Print the help
 */
void cmdline_parser_print_help(void);
/**
 * Print the version
 */
void cmdline_parser_print_version(void);

/**
 * Initializes all the fields a cmdline_parser_params structure 
 * to their default values
 * @param params the structure to initialize
 */
void cmdline_parser_params_init(struct cmdline_parser_params *params);

/**
 * Allocates dynamically a cmdline_parser_params structure and initializes
 * all its fields to their default values
 * @return the created and initialized cmdline_parser_params structure
 */
struct cmdline_parser_params *cmdline_parser_params_create(void);

/**
 * Initializes the passed gengetopt_args_info structure's fields
 * (also set default values for options that have a default)
 * @param args_info the structure to initialize
 */
void cmdline_parser_init (struct gengetopt_args_info *args_info);
/**
 * Deallocates the string fields of the gengetopt_args_info structure
 * (but does not deallocate the structure itself)
 * @param args_info the structure to deallocate
 */
void cmdline_parser_free (struct gengetopt_args_info *args_info);

/**
 * Checks that all the required options were specified
 * @param args_info the structure to check
 * @param prog_name the name of the program that will be used to print
 *   possible errors
 * @return
 */
int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
