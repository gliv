#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include "gliv.h"

typedef gulong timestamp_t;

#define DECLARE_TIMESTAMP(name) timestamp_t name = 0

void touch(timestamp_t * ts);
void reset_timestamp(timestamp_t * ts);
gboolean up_to_date(timestamp_t ts, timestamp_t req);

#endif
