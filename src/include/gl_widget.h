#ifndef GL_WIDGET_H
#define GL_WIDGET_H

#include "gliv.h"

void update_bg_color(void);
void create_gl_widget(void);
gint report_opengl_errors(const gchar * function_name,
                          const gchar * source_name, gint line,
                          gint glBegin_inc);

#endif
