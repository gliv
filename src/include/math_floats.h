#ifndef MATH_FLOATS_H
#define MATH_FLOATS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>

#ifndef HAVE_SQRTF
#define sqrtf sqrt
#endif

#ifndef HAVE_HYPOT
#define hypot(x, y) (sqrtf((x) * (x) + (y) * (y)))
#endif

#ifndef HAVE_HYPOTF
#define hypotf hypot
#endif

#ifndef HAVE_ATAN2F
#define atan2f atan2
#endif

#ifndef HAVE_POWF
#define powf pow
#endif

#ifndef HAVE_CEILF
#define ceilf ceil
#endif

#ifndef HAVE_COSF
#define cosf cos
#endif

#ifndef HAVE_SINF
#define sinf sin
#endif

#ifndef HAVE_ACOSF
#define acosf acos
#endif

#ifndef HAVE_FMODF
#define fmodf fmod
#endif

#ifndef HAVE_FABSF
#define fabsf fabs
#endif

#ifndef HAVE_POWF
#define powf pow
#endif

#ifndef HAVE_LOG10F
#define log10f log10
#endif

/* Instead of finding it in a system or glib header. */
#define PI 3.1415926535897932384626433832795029F

#endif
