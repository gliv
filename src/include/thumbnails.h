#ifndef THUMBNAILS_H
#define THUMBNAILS_H

#include "gliv.h"
#include "tree.h"

gchar *get_absolute_filename(const gchar * filename);
void add_thumbnail(const gchar * filename, GdkPixbuf * pixbuf);
gboolean fill_thumbnail(tree_item * item);
void collection_add_thumbnail(gchar * key, GdkPixbuf * thumb);
GdkPixbuf *get_thumbnail(const gchar * filename, gchar ** thumb_key);

#endif
