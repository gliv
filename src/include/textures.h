#ifndef TEXTURES_H
#define TEXTURES_H

#include "gliv.h"
#include "gliv-image.h"

void prioritize_textures(GlivImage * im, gboolean is_prio);
void create_maps(GlivImage * im);

#endif
