#ifndef HELP_TEXT_H
#define HELP_TEXT_H

#include "gliv.h"

gchar **get_help_lines(void);

#endif
