#ifndef TREE_H
#define TREE_H

#include "gliv.h"
#include "timestamp.h"

typedef struct {
    gchar *name;                /* Displayed name. */
    gchar *path;                /* Associated filename, 'const' on a leaf. */
    gchar *thumb_key;           /* Key for the hash-table. */
    GdkPixbuf *thumb;           /* Thumbnail. */
} tree_item;

void end_using_tree(void);
gboolean cancel_using_tree(void);
gboolean canceled_using_tree(void);
gboolean more_recent_than_tree(timestamp_t ts);
GNode *make_tree(void);
void destroy_tree(GNode * tree);
gint tree_count_files(void);
void invalidate_last_tree(void);

#endif
