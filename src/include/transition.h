#ifndef TRANSITION_H
#define TRANSITION_H

#include "gliv-image.h"

void diff_timeval(GTimeVal * res, GTimeVal * big, GTimeVal * small);
void transition(GlivImage * im);
gboolean is_in_transition(void);

#endif
