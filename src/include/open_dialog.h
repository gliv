#ifndef OPEN_DIALOG_H
#define OPEN_DIALOG_H

#include "gliv.h"

gboolean menu_open(gboolean select_dir);
gboolean menu_image_nr(void);

#endif
