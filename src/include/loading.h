#ifndef LOADING_H
#define LOADING_H

#include "gliv.h"
#include "gliv-image.h"
#include "formats.h"

void start_quiet(void);
void stop_quiet(void);
const gchar *currently_loading(void);
void install_segv_handler(void);
void fill_ident(GlivImage * im);
const gchar *get_extension(const gchar * filename);
loader_t get_loader(const gchar * filename);
void set_loading(const gchar * filename);
gint pixels_size(GdkPixbuf * pixbuf);
GlivImage *make_gliv_image(GdkPixbuf * pixbuf);
GlivImage *load_file(const gchar * filename, GError ** error);

#endif
