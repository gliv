#ifndef LARGE_FILES_H
#define LARGE_FILES_H

#include "gliv.h"

#ifndef HAVE_FTELLO
#define ftello ftell
#endif

#ifndef HAVE_FSEEKO
#define fseeko fseek
#endif

#endif
