#ifndef THREAD_H
#define THREAD_H

#include "gliv.h"

gpointer do_threaded(GThreadFunc f, gpointer arg);

#endif
