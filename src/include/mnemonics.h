#ifndef MNEMONICS_H
#define MNEMONICS_H

#include "gliv.h"

const gchar *add_mnemonic(const gchar * str);
void reset_mnemonics(void);
void push_mnemonics(void);
void pop_mnemonics(void);

#endif
