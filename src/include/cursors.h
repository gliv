#ifndef CURSORS_H
#define CURSORS_H

#include "gliv.h"

gboolean hide_cursor(void);
void show_cursor(void);
gboolean set_correct_cursor(void);
void set_hide_cursor_enabled(gboolean enabled);
void schedule_hide_cursor(void);

#endif
