#ifndef MATRIX_H
#define MATRIX_H

#include "gliv.h"
#include "gliv-image.h"
#include "texture_map.h"

G_GNUC_PURE gfloat get_matrix_zoom(void);
G_GNUC_PURE gfloat get_matrix_angle(void);
G_GNUC_PURE gboolean float_equal(gfloat a, gfloat b);
void get_matrix_bounding_box(gfloat * min_x, gfloat * max_x,
                             gfloat * min_y, gfloat * max_y);
void write_gl_matrix(void);
void matrix_cpy(gfloat * dest, gfloat * src);
gboolean matrix_tile_visible(tile_dim * tile);
G_GNUC_PURE gboolean is_matrix_symmetry(void);
G_GNUC_PURE gboolean get_matrix_has_changed(void);
G_GNUC_PURE gboolean is_filtering_needed(void);
gboolean matrix_set_max_zoom(gint width, gint height, gboolean do_it);
void matrix_fit(gboolean fit_w, gboolean fit_h);
void matrix_reset(void);
void matrix_rotate(gfloat angle);
void matrix_move(gfloat x, gfloat y);
void matrix_zoom(gfloat ratio, gfloat x, gfloat y);
void flip_matrix(gfloat * mat, gboolean h_flip);
void matrix_flip_h(void);
void matrix_flip_v(void);
void matrix_set_initial_position(void);
void configure_matrix(GlivImage * im);
gfloat *new_matrix(void);
gfloat *get_matrix_for_image(GlivImage * im);

#endif
