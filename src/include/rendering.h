#ifndef RENDERING_H
#define RENDERING_H

#include "gliv.h"
#include "gliv-image.h"

/* Bits for the refresh() function. */
#define REFRESH_IMAGE      (1 << 0)
#define REFRESH_NOW        (1 << 1)
#define REFRESH_STATUS     (1 << 2)
#define APPEND_HISTORY     (1 << 3)
#define REFRESH_BURST      (1 << 4)
#define REFRESH_TITLE      (1 << 5)
#define REFRESH_SCROLL     (1 << 6)

void draw_current_image(void);
void render(void);
void zoom_in(gfloat ratio);
gint diff_timeval_us(GTimeVal * after, GTimeVal * before);
gint get_fps_limiter_delay(GTimeVal * previous_frame);
void refresh(gint what);
void update_current_image_status(gboolean find_number);
void do_later(gint priority, GSourceDummyMarshal func);

#endif
