#ifndef PARAMS_H
#define PARAMS_H

/* User configurable settings. */

#define ZOOM_FACTOR      1.1
#define MOVE_OFFSET      20.0
#define BIG_ROTATION     (PI / 2.0)     /* 90 deg */
#define SMALL_ROTATION   (PI / 1800.0)  /* 0.1 deg */
#define MIPMAP_RATIO     0.75

/* Font for the help box. */
#define FONT "Mono 10"

#endif
