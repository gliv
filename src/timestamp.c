/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * See the COPYING file for license information.
 *
 * Guillaume Chazarain <guichaz@gmail.com>
 */

/********************************************
 * Timestamps for important data structures *
 ********************************************/

#include "gliv.h"
#include "timestamp.h"

/* By maintaining our clock we have an instruction level precision. */
static DECLARE_TIMESTAMP(global_clock);
G_LOCK_DEFINE_STATIC(global_clock);

void touch(timestamp_t * ts)
{
    G_LOCK(global_clock);
    *ts = ++global_clock;
    G_UNLOCK(global_clock);
}

void reset_timestamp(timestamp_t * ts)
{
    *ts = 0;
}

gboolean up_to_date(timestamp_t ts, timestamp_t req)
{
    return req && ts > req;
}
