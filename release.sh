#!/bin/bash

set -x
set -e # Exit on error

PACKAGE=$(basename "$PWD")
mkdir dist
TEMPDIR="$(mktemp -d)"
read VERSION < NEWS
echo "$PACKAGE-$VERSION: $TEMPDIR"
mkdir "$TEMPDIR/$PACKAGE-$VERSION"
git archive HEAD | (cd "$TEMPDIR/$PACKAGE-$VERSION" && tar vx)
DIR="$PWD"
cd "$TEMPDIR"
rm "$PACKAGE-$VERSION/release.sh"
tar czf "$DIR/dist/$PACKAGE-$VERSION.tar.gz" "$PACKAGE-$VERSION"
tar cjf "$DIR/dist/$PACKAGE-$VERSION.tar.bz2" "$PACKAGE-$VERSION"
mkdir BUILD RPMS SOURCES SRPMS
ln "$DIR/dist/$PACKAGE-$VERSION.tar.bz2" SOURCES/
unset LINGUAS CFLAGS
(cd "$PACKAGE-$VERSION"
sed -e "s,@VERSION@,$VERSION," -e "s,@prefix@,/usr," gliv.spec.in > gliv.spec)
rpmbuild --define "_topdir $PWD" -ba "$PACKAGE-$VERSION/$PACKAGE.spec"
mv "SRPMS/$PACKAGE-$VERSION-1.src.rpm" "$DIR/dist"
mv "RPMS/"*"/$PACKAGE-$VERSION-1."*".rpm" "$DIR/dist"
rm -fr "$TEMPDIR"
